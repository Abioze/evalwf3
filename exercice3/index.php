<?php
include_once 'init.php';

$query_string = "SELECT id, title, director, year_of_prod FROM movies";

$queryPDO = $pdo->query($query_string);

$results = $queryPDO->fetchAll(PDO::FETCH_OBJ);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Film</title>
    </head>

    <body>

        <table border="1">
            <tr>
                <th>Nom du film</th>
                <th>Réalisateur</th>
                <th>Année</th>
                <th>Plus</th>

            </tr>

            <?php foreach ($results as $film): ?>
            <tr>
                <td><?php echo $film->title ; ?></td>
                <td><?php echo $film->director; ?></td>
                <td><?php echo $film->year_of_prod; ?></td>
                <td><?php echo "<a href=\"film.php?filmId=".$film->id."\">Plus d'infos</a>" ?></td>

            </tr>
            <?php endforeach; ?>
        </table>

        <a href="addfilm.php">Ajouter un film</a>
    </body>

</html>
