<?php
include_once 'init.php';

// On teste l'existance du paramètre "filmId" dans l'url.
// Si celui-ci n'est pas définie, on arrete le programme
if (!isset($_GET['filmId'])) {
    die("le paramètre filmId est manquant.");
}

// Récupération de l'ID du film passé en paramètre dans l'URL
$id_film = $_GET['filmId'];

// On définie la chaine de caractere de la requete
$query_string = "SELECT
        id,
        title,
        actors,
        director,
        producer,
        year_of_prod,
        language,
        category,
        storyline,
        video
    FROM
        `movies` AS t1
    WHERE id=:idFilm";

// On prepare la requete pour PDO
$queryPDO = $pdo->prepare($query_string);

// On définie les "variables" PDO
// :idfilm = $id_film
$queryPDO->bindValue(":idFilm", $id_film, PDO::PARAM_INT);

// On execute la requete
$queryPDO->execute();

// On récupère le resultat de la requete
$result = $queryPDO->fetchAll(PDO::FETCH_OBJ);
var_dump($result);

// On test si le film demandé existe ou non.
// Si le film n'existe pas, le tableau $filmsArray restera vide. On test donc
// si le tableau $filmsArray est vide ou non
if (empty($result)) {
    echo "Le film n'existe pas.";
    exit;
}



// On affiche le nom du film
$film_name = $result[0]->title;
echo "<h1>".$film_name."</h1>";

// On affiche le nom des acteurs
$film_actors = $result[0]->actors;
echo "Acteur(s): ".$film_actors."<br>";

// On affiche le réalisateur
$film_director = $result[0]->director;
echo "Réalisateur: ".$film_director."<br>";

// On affiche le producteur
$film_producer = $result[0]->producer;
echo "Producteur: ".$film_producer."<br>";

// On affiche l'année de production
$film_year_of_prod = $result[0]->year_of_prod;
echo "Année de production: ".$film_year_of_prod."<br>";

// On affiche la langue
$film_language = $result[0]->language;
echo "Langue: ".$film_language."<br>";

// On affiche la categorie
$film_category = $result[0]->category;
echo "Catégorie: ".$film_category."<br>";

// On affiche le synopsis
$film_storyline = $result[0]->storyline;
echo "Synopsis: ".$film_storyline."<br>";

// On affiche le lien de la bande annonce
$film_video = $result[0]->video;
echo "Bande annonce: "."<a href=\"".$film_video."\">".$film_video."</a><br>";

echo "<a href=\"index.php\">Retour aux films</a>";
