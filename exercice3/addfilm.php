<?php
include_once 'init.php';

// Definition des variables par défaut
$title = null;
$actors = null;
$director = null;
$producer = null;
$yeah_of_prod = null;
$language = null;
$categories = null;
$synopsis = null;
$video = null;


// On controle si l'utilisateur envois le formulaire d'inscription
if (isset($_POST['sendfilm'])) {

    // Récupération des données du formulaire
    $title          = isset($_POST['title']) ? trim($_POST['title']) : null;
    $actors         = isset($_POST['actors']) ? $_POST['actors'] : null;
    $director       = isset($_POST['director']) ? trim($_POST['director']) : null;
    $producer       = isset($_POST['producer']) ? trim($_POST['producer']) : null;
    $year_of_prod   = isset($_POST['year_of_prod']) ? trim($_POST['year_of_prod']) : null;
    $language       = isset($_POST['language']) ? trim($_POST['language']) : null;
    $category     = isset($_POST['categories']) ? trim($_POST['categories']) : null;
    $synopsis       = isset($_POST['synopsis']) ? trim($_POST['synopsis']) : null;
    $video          = isset($_POST['video']) ? trim($_POST['video']) : null;

    $send = true;

    if (strlen($title) < 5) {
        $send = false;
        echo "<p style=\"color:red\">Le titre doit comporter 5 caractères minimum.</p>";
    }
    elseif (strlen($actors) < 5) {
        $send = false;
        echo "<p style=\"color:red\">Le nom du/des acteur(s) doit comporter 5 caractères minimum.</p>";
    }
    elseif (strlen($director) < 5) {
        $send = false;
        echo "<p style=\"color:red\">Le nom du réalisateur doit comporter 5 caractères minimum.</p>";
    }
    elseif (strlen($producer) < 5) {
        $send = false;
        echo "<p style=\"color:red\">Le nom du producteur doit comporter 5 caractères minimum.</p>";
    }
    elseif (strlen($synopsis) < 5) {
        $send = false;
        echo "<p style=\"color:red\">Le synopsis doit comporter 5 caractères minimum.</p>";
    }

    // Controle si l'url de la video est valide
    elseif (!filter_var($video, FILTER_VALIDATE_URL)) {
        $send = false;
        echo "<p style=\"color:red\">Le lien de la vidéo n'est pas valide.</p>";
    }

    // Controle de l'existance du film dans le BDD
    if ($send) {
        $q = "SELECT id FROM `movies` WHERE title=:title";
        $q = $pdo->prepare($q);
        $q->bindValue(":title", $title, PDO::PARAM_STR);
        $q->execute();
        if ($q->fetch(PDO::FETCH_OBJ)) {
        // if (true) {
            $send = false;
            echo "<p style=\"color:red\">Un film a déjà été enregistré avec le nom $title.</p>";
        }
        $q->closeCursor();
    }

    if ($send) {
        // Enregistrement de l'utilusateur
        $q = "INSERT INTO `movies` (`title`,`actors`,`director`,`producer`,`year_of_prod`,`language`,`category`,`storyline`,`video`)
                            VALUES (:title, :actors, :director, :producer, :year_of_prod, :language, :category, :storyline, :video)";
        $q = $pdo->prepare($q);
        $q->bindValue(":title", $title, PDO::PARAM_STR);
        $q->bindValue(":actors", $actors, PDO::PARAM_STR);
        $q->bindValue(":director", $director, PDO::PARAM_STR);
        $q->bindValue(":producer", $producer, PDO::PARAM_STR);
        $q->bindValue(":year_of_prod", $year_of_prod, PDO::PARAM_INT);
        $q->bindValue(":language", $language, PDO::PARAM_STR);
        $q->bindValue(":category", $category, PDO::PARAM_STR);
        $q->bindValue(":storyline", $synopsis, PDO::PARAM_STR);
        $q->bindValue(":video", $video, PDO::PARAM_STR);
        $q->execute();
        $q->closeCursor();

        echo "Votre film a bien été ajouté.";

        header("refresh:5;url=index.php");
    }

}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Film</title>
</head>
<body>
    <h1>Ajouter un film</h1>

    <form  method="post">

                <div>
                    <label for="title">Nom du film</label><br>
                    <input type="text" id="title" name="title" value="">
                </div>
                <div>
                    <label for="actors">Noms des acteurs</label><br>
                    <input type="text" id="actors" name="actors" value="">
                </div>

                <div>
                    <label for="director">Nom du réalisateur</label><br>
                    <input type="text" id="director" name="director" value="">
                </div>

                <div>
                    <label for="producer">Nom du producteur</label><br>
                    <input type="text" id="producer" name="producer" value="">
                </div>

                <div>
                    <label for="year_of_prod">Année de production</label><br>
                    <select name="year_of_prod" id="year_of_prod">
                         <?php for($i= date('Y'); $i>=date('Y')-100; $i--): ?>
                         <option value="<?php echo $i; ?>"><?php
                          echo $i;
                          ?></option>
                         <?php endfor; ?>
                    </select>
                </div>

                <div>
                    <label for="language">Langue</label><br>
                    <select  name="language" id="language">
                        <option value="language">Français</option>
                        <option value="language">Anglais</option>
                        <option value="language">Espagnol</option>
                        <option value="language">Chinois</option>
                        <option value="language">Allemand</option>
                    </select>
                </div>

                <div>
                    <label for="categories">Catégories</label><br>
                    <select  name="categories" id="categories">
                        <option value="categories">Action</option>
                        <option value="categories">Comédie</option>
                        <option value="categories">Drame</option>
                        <option value="categories">Horreur</option>
                        <option value="categories">Science-fiction</option>
                    </select>
                </div>

                <div>
                    <label for="synopsis">Synopsis</label><br>
                    <textarea name="synopsis" id="synopsis" rows="8" cols="80"></textarea>
                </div>

                <div >
                    <label for="video">Lien de la bande annonce</label><br>
                    <input type="text" id="video" name="video" value="">
                </div>

                <button type="submit" name="sendfilm">Ajouter</button>
            </form>
</body>
</html>
