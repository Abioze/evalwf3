<?php

// Tableau PHP des informations
$arrayUser = array(
        "Prénom"        => "Gaëtan",
        "Nom"           => "BELART",
        "Adresse"       => "105 Avenue de la république",
        "Code Postal"   => "59110",
        "Ville"         => "La Madeleine",
        "Email"         => "belart@wayne-enterprise.com",
        "Téléphone"     => "0777889966",
        "Anniversaire"  => "1967-12-17"
    );

 ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>On se présente</title>
    </head>

    <body>
        <h1>On se présente</h1>

        <ul>

            <!-- Boucle du tableau des informations dans une liste HTML -->
            <?php foreach ($arrayUser as $line => $value) : ?>
                <li>
                    <?php if( $line == "Anniversaire") { // Condition de formatage du format de la date en format Francais.
                        $date = new DateTime($value);
                        echo $line." : ". $date->format('d/m/Y');
                    } else {
                        echo $line." : ". $value;
                    } ?>
                </li>
        <?php endforeach; ?> <!-- fin de la boucle -->
        </ul>

    </body>
</html>
