<?php
$euro = null;

// Controle du formulaire
if (isset($_POST['validate'])) {


    $euro = isset($_POST['euro']) ? trim($_POST['euro']) : null;


    // On verifie que le champ euros n'est pas vide.
    if (empty($euro)) {
        echo "Veuillez entrer une valeur en euros.";
    }

    // On verifie que le champ euros est de type numérique.
    elseif (!is_numeric($euro)) {
        echo "Veuillez entrer une valeur numérique.";
    }

    elseif ($euro < 0) {
        echo "Veuillez entrer une valeur positive.";
    }
    // Fonction de conversion des euros en dollars.
    function convert($euros,$exchange)
    {
        $euros = $euros*$exchange;
        return $euros;
    }
    $convert = convert($_POST['euro'],1.1457);
}
 ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>On part en voyage</title>
    <meta name="description" content="">
  </head>
    <body>

        <h3>Convertisseur des euros en dollars américains.</h3>

        <form method="POST" name="convertEurUsd">
            <input type="text" name="euro" id="valeur" placeholder="Entrez une valeur en euros.">
            <input type="submit" name="validate" value="Convertir">
        </form>

        <!-- Affichage du resultat de la conversion -->
        <!-- Si rien n'est defini, ou la valeur n'est pas numérique ou non supérieure
        à 0, rien ne s'affiche -->
        <?php
        global $convert;
        if (isset($convert) && is_numeric($euro) && ($euro > 0)){
        echo $euro."€ = ".$convert."$";
        }
        ?>
    </body>
</html>
